<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;


Route::group([
    'prefix' => 'admin',
    'namespace' => 'Admin'
], function () {
    Auth::routes([
        'register' => false,
        'reset' => false,
        'verify' => false
    ]);

    Route::group([
        'middleware' => ['auth']
    ], function () {
        Route::get('/', 'HomeController@index')->name('admin.home');
        Route::match(['GET','POST'],'/page/about', 'PageController@about')->name('admin.page.about');
        Route::match(['GET','POST'],'/page/contact', 'PageController@contact')->name('admin.page.contact');
        Route::match(['GET', 'POST'], '/registration_year', 'RegistrationYearController@index')
            ->name('admin.registration.year.index');
        Route::post( '/registration_year/add', 'RegistrationYearController@add')
            ->name('admin.registration.year.add');
        Route::post( '/registration_year/edit/{registration_year}', 'RegistrationYearController@edit')
            ->name('admin.registration.year.edit');
        Route::post( '/registration_year/delete', 'RegistrationYearController@delete')
            ->name('admin.registration.year.delete');
        Route::post( '/registration_year/status/{registration_year}', 'RegistrationYearController@status')
            ->name('admin.registration.year.status');

        Route::match(['GET', 'POST'], '/registrant', 'RegistrantController@index')
            ->name('admin.registrant.index');
        Route::match(['GET', 'POST'], '/registrant/list/{registration_year}', 'RegistrantController@list')
            ->name('admin.registrant.list');
        Route::get( '/registrant/detail/{registrant}', 'RegistrantController@detail')
            ->name('admin.registrant.detail');
        Route::get( '/registrant/generate/{registration_year}', 'RegistrantController@generate')
            ->name('admin.registrant.generate');

        Route::match(['GET', 'POST'], '/students', 'StudentController@index')
            ->name('admin.students.index');
        Route::match(['GET', 'POST'], '/students/list/{registration_year}', 'StudentController@list')
            ->name('admin.students.list');
    });
});

Route::get('/', 'HomeController@index')->name('home');
Route::get('/about', 'HomeController@about')->name('about');
Route::get('/contact', 'HomeController@contact')->name('contact');
Route::match(['POST', 'GET'], '/admissions', 'HomeController@admissions')->name('admissions');
