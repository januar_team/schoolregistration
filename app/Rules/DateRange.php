<?php

namespace App\Rules;

use DateTime;
use Illuminate\Contracts\Validation\Rule;

class DateRange implements Rule
{
    private $format;
    private $attribut;

    /**
     * Create a new rule instance.
     *
     * @param $format
     */
    public function __construct($format)
    {
        $this->format = $format;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $this->attribut = $attribute;
        $values = explode('-', $value);

        if (count($values) != 2)
            return false;

        return $this->validateDateFormat(trim($values[0]), $this->format) &&
            $this->validateDateFormat(trim($values[1]), $this->format);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('validation.date_format', ['attribute' => $this->attribut, 'format' => $this->format]);
    }

    private function validateDateFormat($value, $format)
    {
        if (! is_string($value) && ! is_numeric($value)) {
            return false;
        }

        $date = DateTime::createFromFormat('!'.$format, $value);

        return $date && $date->format($format) == $value;
    }
}
