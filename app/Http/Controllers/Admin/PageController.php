<?php

namespace App\Http\Controllers\Admin;

use App\Models\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
    public function about(Request $request){
        $page = Page::where('name', '=', 'about')->first();

        if ($request->isMethod('POST')){
            $page->value = $request->page;
            $page->save();
        }

        return $this->view([
            'page' => $page
        ]);
    }

    public function contact(Request $request){
        $page = Page::where('name', '=', 'contact')->first();

        if ($request->isMethod('POST')){
            $page->value = $request->page;
            $page->save();
        }

        return $this->view([
            'page' => $page
        ]);
    }
}
