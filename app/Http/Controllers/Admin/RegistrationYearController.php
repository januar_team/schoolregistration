<?php

namespace App\Http\Controllers\Admin;

use App\Models\RegistrationYear;
use App\Rules\DateRange;
use DateTime;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Yajra\DataTables\Facades\DataTables;

class RegistrationYearController extends Controller
{
    public function index(Request $request)
    {
        if ($request->isMethod('POST')) {
            $registration_years = RegistrationYear::all();

            return DataTables::of($registration_years)->make();
        }

        return $this->view();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function add(Request $request)
    {
        $format = 'd/m/Y';
        $this->validate($request, [
            'name' => 'required|unique:registration_years',
            'duration' => ['required', new DateRange($format)],
            'max_registrant' => 'required|numeric|min:',
            'class_count' => 'required|numeric',
            'min_student_count' => 'required|numeric'
        ]);

        $registration_year = new RegistrationYear($request->all());
        $dates = explode('-', $request->duration);
        $registration_year->start_date = DateTime::createFromFormat('!'.$format, trim($dates[0]));
        $registration_year->end_date = DateTime::createFromFormat('!'.$format, trim($dates[1]));
        $registration_year->save();

        return $this->json();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function edit(Request $request, RegistrationYear $registration_year)
    {
        $format = 'd/m/Y';
        $this->validate($request, [
            'name' => ['required', Rule::unique('registration_years')->ignore($registration_year->id)],
            'duration' => ['required', new DateRange($format)],
            'max_registrant' => 'required|numeric|min:',
            'class_count' => 'required|numeric',
            'min_student_count' => 'required|numeric'
        ]);

        $registration_year->fill($request->all());
        $dates = explode('-', $request->duration);
        $registration_year->start_date = DateTime::createFromFormat('!'.$format, trim($dates[0]));
        $registration_year->end_date = DateTime::createFromFormat('!'.$format, trim($dates[1]));
        $registration_year->save();

        return $this->json();
    }


    public function delete(Request $request){
        $registration_year = RegistrationYear::find($request->id);

        if ($registration_year){
            $registration_year->delete();
        }

        return $this->json();
    }

    public function status(Request $request, RegistrationYear $registration_year)
    {
        DB::transaction(function() use($registration_year, $request){
            DB::table('registration_years')->update(['status' => false]);
            $registration_year->status = $request->status == "true" ? 1 : 0;
            $registration_year->save();
        });

        return $this->json();
    }
}
