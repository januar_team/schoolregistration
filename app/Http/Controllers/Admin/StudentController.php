<?php

namespace App\Http\Controllers\Admin;

use App\Models\RegistrationYear;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;

class StudentController extends Controller
{
    public function index(Request $request){
        if ($request->isMethod('POST')) {
            $registration_years = RegistrationYear::withCount('students');

            return DataTables::of($registration_years)->make();
        }

        return $this->view();
    }

    public function list(Request $request, RegistrationYear $registration_year){
        if ($request->isMethod('POST')) {
            $students = $registration_year->students()->with('registrant');

            return DataTables::of($students)->make();
        }

        return $this->view([
            'registration_year' => $registration_year
        ]);
    }
}
