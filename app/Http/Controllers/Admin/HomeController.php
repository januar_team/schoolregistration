<?php

namespace App\Http\Controllers\Admin;

use App\Models\RegistrationYear;
use App\Models\Student;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index(Request $request){
        $registration_year = RegistrationYear::where('status', '=', 1)->first();
        $registration_last = RegistrationYear::orderBy('id', 'DESC')->first();
        $data = [
            'current_registrant' => ($registration_year) ? $registration_year->registrants()->count() : 0,
            'students'  => Student::count(),
            'students_current' => ($registration_last) ? $registration_last->students()->count() : 0
        ];

        return $this->view($data);
    }
}
