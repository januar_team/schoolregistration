<?php

namespace App\Http\Controllers\Admin;

use App\Models\Registrant;
use App\Models\RegistrationYear;
use App\Models\Student;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class RegistrantController extends Controller
{
    public function index(Request $request){
        if ($request->isMethod('POST')) {
            $registration_years = RegistrationYear::withCount('registrants');

            return DataTables::of($registration_years)->make();
        }

        return $this->view();
    }

    public function list(Request $request, RegistrationYear $registration_year){
        if ($request->isMethod('POST')) {
            $registrants = $registration_year->registrants();

            return DataTables::of($registrants)->make();
        }

        return $this->view([
            'registration_year' => $registration_year
        ]);
    }

    public function detail(Request $request, Registrant $registrant){
        return $this->view([
            'registrant' => $registrant
        ]);
    }

    public function generate(Request $request, RegistrationYear $registration_year){
        DB::transaction(function () use($registration_year){
            $count = $registration_year->registrants()->count();
            $class_count = $registration_year->class_count;
            while (($count / $class_count) < $registration_year->min_student_count){
                $class_count--;
            }

            $class = 1;
            foreach ($registration_year->registrants as $registrant) {
                $student = new Student([
                    'registration_year_id'  => $registration_year->id,
                    'registrant_id'         => $registrant->id,
                    'class'                 => 'X'.$class
                ]);
                $student->save();

                $class = ($class >= $registration_year->class_count) ? 1 : ($class + 1);
            }
        });

        return redirect(route('admin.students.list', $registration_year->id));
    }
}
