<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Route;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @var JsonResponse
     */
    protected $response;

    /**
     * @var int
     */
    protected $code;

    public function __construct()
    {
        $this->code = 200;
        $this->response = new JsonResponse();
    }

    protected function json($data = null, $code = null){
        $data = $data?:$this->response;
        $code = $code?:$this->code;

        return response()->json($data, $code);
    }

    protected function view($data = [], $view = ''){
        if ($view == ''){
            $currentAction = Route::getCurrentRoute()->getActionName();
            list($controller, $method) = explode('@', $currentAction);
            $controller = str_replace("App\Http\Controllers\\", "", $controller);
            $controller = explode('\\', $controller);
            $controller[count($controller) - 1] = preg_replace('/Controller$/', '',end($controller));
            $route = array_merge($controller, [$method]);
            $view = strtolower(implode($route, '.'));
        }

        return view($view, $data);
    }
}
