<?php

namespace App\Http\Controllers;

use App\Models\Page;
use App\Models\Registrant;
use App\Models\RegistrationYear;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return $this->view();
    }

    public function about(){
        $page = Page::where('name', 'about')->first();
        return $this->view(['page' => $page]);
    }

    public function contact(){
        $page = Page::where('name', 'contact')->first();
        return $this->view(['page' => $page]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Validation\ValidationException
     */
    public function admissions(Request $request){
        $date = Carbon::now();
        $registration_year = RegistrationYear::where('status', '=', 1)
            ->where('start_date', '<=',$date->format('Y-m-d'))
            ->where('end_date', '>=',$date->format('Y-m-d'))->first();

        if ($registration_year && $request->isMethod('POST')){
            $this->validate($request, [
                'name'               => 'required',
                'nickname'          => 'required',
                'nisn'              => 'required|unique:registrants',
                'place_of_birth'    => 'required',
                'date_of_birth'     => 'required|date',
                'gender'            => 'required',
                'religion'          => 'required|in:Islam, Kristen Protestan, Katolik, Hindu, Budha, Konghucu',
                'nationality'       => 'required',
                'child_num'         => 'required|numeric',
                'child_of'          => 'required|numeric',
                'family_status'     => 'required',
                'address'           => 'required',
                'rt_rw'             => 'required',
                'village'           => 'required',
                'district'          => 'required',
                'city'              => 'required',
                'province'          => 'required',
                'post_code'         => 'required',
                'school_from'       => 'required',
                'school_from_address'   => 'required',
                'sttb'              => 'required|unique:registrants',
                'graduation_year'   => 'required|numeric|min:' . Carbon::now()->subYears(3)->year . '|max:' . Carbon::now()->year,
                'skhun'             => 'required|unique:registrants',
                'sttb_mark'         => 'required|numeric',
                'father_name'       => 'required',
                'father_place_of_birth' => 'required',
                'father_date_of_birth' => 'required|date',
                'father_religion'   => 'required|in:Islam, Kristen Protestan, Katolik, Hindu, Budha, Konghucu',
                'father_job'        => 'required',
                'father_address'    => 'required',
                'mother_name'       => 'required',
                'mother_place_of_birth' => 'required',
                'mother_date_of_birth' => 'required|date',
                'mother_religion'   => 'required|in:Islam, Kristen Protestan, Katolik, Hindu, Budha, Konghucu',
                'mother_job'        => 'required',
                'mother_address'    => 'required',
            ]);

            $registrant = new Registrant($request->all());
            $last_registrant = Registrant::where('registration_year_id', $registration_year->id)
                ->orderBy('id', 'DESC')->first();
           $registrant->registration_number = ($last_registrant) ? sprintf('%04d',
                (int)str_replace($registration_year->id . "-", "", $last_registrant->registration_number) + 1) : sprintf('%04d', 1);
            $registrant->registration_number = $registration_year->id . "-" . $registrant->registration_number;
            $registrant->registration_year_id = $registration_year->id;
            $registrant->save();

            return $this->view([
                'registration_year' => $registration_year,
                'success'    => true,
                'registrant' => $registrant
            ]);
        }

        return $this->view([
            'registration_year' => $registration_year
        ]);
    }
}
