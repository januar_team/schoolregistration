<?php

namespace App\Console\Commands;

use App\Models\Registrant;
use App\Models\RegistrationYear;
use Faker\Factory;
use Illuminate\Console\Command;

class GenerateRegistrant extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:registrant {registration_year} {{--total=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Registrant Data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $registration_year_id = $this->argument('registration_year');
        $total = $this->option('total')?:100;

        $registration_year = RegistrationYear::find($registration_year_id);

        if (is_null($registration_year)){
            $this->error(sprintf('Registration Year with id %s not found', $registration_year_id));
            return;
        }

        $faker = Factory::create();

        $last_registrant = Registrant::where('registration_year_id', $registration_year->id)
            ->orderBy('id', 'DESC')->first();
        $registration_number = ($last_registrant) ? (int)str_replace($registration_year->id . "-", "", $last_registrant->registration_number) + 1 : 1;
        $unique = '1221';
        $unique_start = 3;
        $gender = [
            'laki-laki',
            'perempuan'
        ];

        $religion = [
            'Islam', 'Kristen Protestant', 'Katolik', 'Hindu', 'Buddha', 'Konghucu'
        ];

        for ($i = 0; $i < $total; $i++){
            $child_num = rand(1, 5);
            Registrant::create([
                'name'                  => $faker->name,
                'registration_number'   => $registration_year->id . "-" . sprintf('%04d', $registration_number),
                'registration_year_id'  => $registration_year->id,
                'nickname'              => $faker->firstName,
                'nisn'                  => $unique . sprintf('%04d', $unique_start),
                'place_of_birth'        => $faker->city,
                'date_of_birth'         => $faker->dateTimeBetween('-16 years', '-14 years')->format('d/m/Y'),
                'gender'                => $gender[rand(0,1)],
                'religion'              => $religion[rand(0, 5)],
                'nationality'           => 'WNI',
                'child_num'             => rand(1, $child_num),
                'child_of'              => $child_num,
                'family_status'         => 'Anak Kandung',
                'address'               => $faker->streetAddress,
                'rt_rw'                 => $faker->streetName,
                'village'               => $faker->streetName,
                'district'              => $faker->streetName,
                'city'                  => $faker->city,
                'province'              => $faker->city,
                'post_code'             => $faker->postcode,
                'school_from'           => 'SMP NEGERI ' . rand(1,10),
                'school_from_address'   => $faker->address,
                'sttb'                  => $registration_year->id . "-" . sprintf('%04d', $registration_number),
                'graduation_year'       => 2019,
                'skhun'                 => $registration_year->id . "-" . sprintf('%04d', $registration_number),
                'sttb_mark'             => rand(30, 40),
                'father_name'           => $faker->name('male'),
                'father_place_of_birth' => $faker->city,
                'father_date_of_birth'  => $faker->dateTimeBetween('-50 years', '-25 years')->format('d/m/Y'),
                'father_religion'       => $religion[rand(0, 5)],
                'father_job'            => $faker->jobTitle,
                'father_address'        => $faker->address,
                'mother_name'           => $faker->name('female'),
                'mother_place_of_birth' => $faker->city,
                'mother_date_of_birth'  => $faker->dateTimeBetween('-50 years', '-25 years')->format('d/m/Y'),
                'mother_religion'       => $religion[rand(0, 5)],
                'mother_job'            => $faker->jobTitle,
                'mother_address'        => $faker->address,
            ]);

            $registration_number++;
            $unique_start++;
        }
    }
}
