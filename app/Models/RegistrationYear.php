<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RegistrationYear extends Model
{
    protected $fillable = ['name','start_date','end_date','status','max_registrant','class_count','min_student_count'];

    protected $casts = [
        'start_date' => 'datetime:d/m/Y',
        'end_date' => 'datetime:d/m/Y'
    ];

    public function getStatusAttribute($value){
        return ($value === 1) ? true :false;
    }

    public function registrants(){
        return $this->hasMany(Registrant::class);
    }

    public function students(){
        return $this->hasMany(Student::class);
    }
}
