<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = ['registration_year_id', 'registrant_id', 'class'];

    public function registrant(){
        return $this->belongsTo(Registrant::class);
    }
}
