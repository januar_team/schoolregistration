<?php

namespace App\Models;

use DateTime;
use Illuminate\Database\Eloquent\Model;

class Registrant extends Model
{
    protected $fillable = [
        'name',
        'registration_year_id',
        'registration_number',
        'nickname',
        'nisn',
        'place_of_birth',
        'date_of_birth',
        'gender',
        'religion',
        'nationality',
        'child_num',
        'child_of',
        'family_status',
        'address',
        'rt_rw',
        'village',
        'district',
        'city',
        'province',
        'post_code',
        'school_from',
        'school_from_address',
        'sttb',
        'graduation_year',
        'skhun',
        'sttb_mark',
        'father_name',
        'father_place_of_birth',
        'father_date_of_birth',
        'father_religion',
        'father_job',
        'father_address',
        'mother_name',
        'mother_place_of_birth',
        'mother_date_of_birth',
        'mother_religion',
        'mother_job',
        'mother_address',
    ];

    protected $casts = [
        'date_of_birth' => 'datetime:d/m/Y',
        'father_date_of_birth' => 'datetime:d/m/Y',
        'mother_date_of_birth' => 'datetime:d/m/Y',
    ];

    private $format = 'd/m/Y';

    public function setDateOfBirthAttribute($value)
    {
        $this->attributes['date_of_birth'] = $this->parseDate($value);
    }

    public function setFatherDateOfBirthAttribute($value)
    {
        $this->attributes['father_date_of_birth'] = $this->parseDate($value);
    }

    public function setMotherDateOfBirthAttribute($value)
    {
        $this->attributes['mother_date_of_birth'] = $this->parseDate($value);
    }

    private function parseDate($value)
    {
        if (! is_string($value) && ! is_numeric($value)) {
            return false;
        }

        $date = DateTime::createFromFormat('!'.$this->format, $value);

        return ($date && $date->format($this->format) == $value) ? $date : $value;
    }

    public function student(){
        return $this->hasOne(Student::class);
    }
}
