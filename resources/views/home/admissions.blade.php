@extends('layouts.app')

@section('content')
    <div class="site-section ftco-subscribe-1 site-blocks-cover pb-4" style="background-image: url('images/bg_1.jpg')">
        <div class="container">
            <div class="row align-items-end">
                <div class="col-lg-7">
                    <h2 class="mb-0">Pendaftaran</h2>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing.</p>
                </div>
            </div>
        </div>
    </div>


    <div class="custom-breadcrumns border-bottom">
        <div class="container">
            <a href="index.html">Beranda</a>
            <span class="mx-3 icon-keyboard_arrow_right"></span>
            <span class="current">Pendaftaran</span>
            <span class="mx-3 icon-keyboard_arrow_right"></span>
            <span class="current">Tahun Ajaran {{($registration_year) ? $registration_year->name : ''}}</span>
        </div>
    </div>

    <div class="container pt-4 mb-4">
        @if(!isset($registration_year))
            <div class="row">
                <div class="col-lg-12">
                    <div class="feature-1 border">
                        <div class="feature-1-content">
                            <h2>Maaf ...</h2>
                            <p>Saat ini tidak ada sekolah tidak membuka pendaftaran siswa baru.</p>
                            <p><a href="/" class="btn btn-primary px-4 rounded-0">Beranda</a></p>
                        </div>
                    </div>
                </div>
            </div>
        @else
            @if(isset($success) && $success)
                <div class="row">
                    <div class="col-lg-12">
                        <div class="feature-1 border">
                            <div class="feature-1-content">
                                <h2>Selamat</h2>
                                <p>Anda berhasil melakukan pendaftaran dengan nomor pendaftaran <b class="primary">{{$registrant->registration_number}}</b>.</p>
                                <p><a href="/" class="btn btn-primary px-4 rounded-0">Beranda</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            @else
                <form method="post">
                    @csrf
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="section-title-underline mb-2">
                                <span>Data Utama Siswa</span>
                            </h4>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4">Nama Siswa Lengkap (sesuai akte kelahiran)</label>
                        <div class="col-md-8">
                            <input type="text" name="name" value="{{old('name')}}"
                                   class="form-control form-control-lg @error('name') is-invalid @enderror">
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4">Nama Panggilan</label>
                        <div class="col-md-8">
                            <input type="text" name="nickname" value="{{old('nickname')}}"
                                   class="form-control form-control-lg @error('nickname') is-invalid @enderror">
                            @error('nickname')
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4">No. Induk Siswa Nasional (NISN)</label>
                        <div class="col-md-8">
                            <input type="text" name="nisn" value="{{old('nisn')}}"
                                   class="form-control form-control-lg @error('nisn') is-invalid @enderror">
                            @error('nisn')
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="section-title-underline mb-2">
                                <span>Data Umum Siswa</span>
                            </h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-md-4">Tempat Lahir</label>
                                <div class="col-md-8">
                                    <input type="text" name="place_of_birth" value="{{old('place_of_birth')}}"
                                           class="form-control form-control-lg @error('place_of_birth') is-invalid @enderror">
                                    @error('place_of_birth')
                                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4">Tanggal Lahir</label>
                                <div class="col-md-8">
                                    <input type="text" name="date_of_birth" value="{{old('date_of_birth')}}"
                                           class="form-control form-control-lg date-picker @error('date_of_birth') is-invalid @enderror">
                                    @error('date_of_birth')
                                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4">Jenis Kelamin</label>
                                <div class="col-md-8">
                                    <select name="gender" class="form-control form-control-lg">
                                        <option value="laki-laki">Laki-Laki</option>
                                        <option value="perempuan">Perempuan</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4">Agama</label>
                                <div class="col-md-8">
                                    <select name="religion" class="form-control form-control-lg">
                                        <option value="Islam">Islam</option>
                                        <option value="Kristen Protestan">Kristen Protestan</option>
                                        <option value="Katolik">Katolik</option>
                                        <option value="Hindu">Hindu</option>
                                        <option value="Buddha">Buddha</option>
                                        <option value="Konghucu">Konghucu</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4">Kewarganegaraan</label>
                                <div class="col-md-8">
                                    <select name="nationality" class="form-control form-control-lg">
                                        <option value="wni">WNI</option>
                                        <option value="wna">WNA</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4">Anak Ke-</label>
                                <div class="col-md-3">
                                    <input type="text" name="child_num" value="{{old('child_num')}}"
                                           class="form-control form-control-lg @error('child_num') is-invalid @enderror">
                                    @error('child_num')
                                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                    @enderror
                                </div>
                                <label class="col-md-2">dari</label>
                                <div class="col-md-3">
                                    <input type="text" name="child_of" value="{{old('child_of')}}"
                                           class="form-control form-control-lg @error('child_of') is-invalid @enderror">
                                    @error('child_of')
                                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4">Status Dalam Keluarga</label>
                                <div class="col-md-8">
                                    <input type="text" name="family_status" value="{{old('family_status')}}"
                                           class="form-control form-control-lg @error('family_status') is-invalid @enderror">
                                    @error('family_status')
                                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-md-4">Alamat</label>
                                <div class="col-md-8">
                            <textarea name="address" cols="30" rows="3"
                                      class="form-control @error('address') is-invalid @enderror">{{old('address')}}</textarea>
                                    @error('address')
                                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4">RT/RW</label>
                                <div class="col-md-8">
                                    <input type="text" name="rt_rw" value="{{old('rt_rw')}}"
                                           class="form-control form-control-lg @error('rt_rw') is-invalid @enderror">
                                    @error('rt_rw')
                                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4">Kelurahan</label>
                                <div class="col-md-8">
                                    <input type="text" name="village" value="{{old('village')}}"
                                           class="form-control form-control-lg @error('village') is-invalid @enderror">
                                    @error('village')
                                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4">Kecamatan</label>
                                <div class="col-md-8">
                                    <input type="text" name="district" value="{{old('district')}}"
                                           class="form-control form-control-lg @error('district') is-invalid @enderror">
                                    @error('district')
                                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4">Kota/Kabupatan</label>
                                <div class="col-md-8">
                                    <input type="text" name="city" value="{{old('city')}}"
                                           class="form-control form-control-lg @error('city') is-invalid @enderror">
                                    @error('city')
                                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4">Provinsi</label>
                                <div class="col-md-8">
                                    <input type="text" name="province" value="{{old('province')}}"
                                           class="form-control form-control-lg @error('province') is-invalid @enderror">
                                    @error('province')
                                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4">Kode Pos</label>
                                <div class="col-md-8">
                                    <input type="text" name="post_code" value="{{old('post_code')}}"
                                           class="form-control form-control-lg @error('post_code') is-invalid @enderror">
                                    @error('post_code')
                                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="section-title-underline mb-2">
                                <span>Data Riwayat Pendidikan</span>
                            </h4>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4">Sekolah Asal</label>
                        <div class="col-md-8">
                            <input type="text" name="school_from" value="{{old('school_from')}}"
                                   class="form-control form-control-lg @error('school_from') is-invalid @enderror">
                            @error('school_from')
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4">Alamat Sekolah Asal</label>
                        <div class="col-md-8">
                            <input type="text" name="school_from_address" value="{{old('school_from_address')}}"
                                   class="form-control form-control-lg @error('school_from_address') is-invalid @enderror">
                            @error('school_from_address')
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4">No. STTB</label>
                        <div class="col-md-8">
                            <input type="text" name="sttb" value="{{old('sttb')}}"
                                   class="form-control form-control-lg @error('sttb') is-invalid @enderror">
                            @error('sttb')
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4">Tahun Lulus</label>
                        <div class="col-md-8">
                            <input type="text" name="graduation_year" value="{{old('graduation_year')}}"
                                   class="form-control form-control-lg @error('graduation_year') is-invalid @enderror">
                            @error('graduation_year')
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4">Nomor SKHUN</label>
                        <div class="col-md-8">
                            <input type="text" name="skhun" value="{{old('skhun')}}"
                                   class="form-control form-control-lg @error('skhun') is-invalid @enderror">
                            @error('skhun')
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-4">Jumlah Nilai STTB/ Rata-Rata</label>
                        <div class="col-md-8">
                            <input type="text" name="sttb_mark" value="{{old('sttb_mark')}}"
                                   class="form-control form-control-lg @error('sttb_mark') is-invalid @enderror">
                            @error('sttb_mark')
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="section-title-underline mb-2">
                                        <span>Identitas Orang Tua (Ayah)</span>
                                    </h4>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4">Nama Ayah</label>
                                <div class="col-md-8">
                                    <input type="text" name="father_name" value="{{old('father_name')}}"
                                           class="form-control form-control-lg @error('father_name') is-invalid @enderror">
                                    @error('father_name')
                                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4">Tempat Lahir</label>
                                <div class="col-md-8">
                                    <input type="text" name="father_place_of_birth"
                                           value="{{old('father_place_of_birth')}}"
                                           class="form-control form-control-lg @error('father_place_of_birth') is-invalid @enderror">
                                    @error('father_place_of_birth')
                                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4">Tanggal Lahir</label>
                                <div class="col-md-8">
                                    <input type="text" name="father_date_of_birth"
                                           value="{{old('father_date_of_birth')}}"
                                           class="form-control form-control-lg @error('father_date_of_birth') is-invalid @enderror">
                                    @error('father_date_of_birth')
                                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4">Agama</label>
                                <div class="col-md-8">
                                    <select name="father_religion" class="form-control form-control-lg">
                                        <option value="Islam">Islam</option>
                                        <option value="Kristen Protestan">Kristen Protestan</option>
                                        <option value="Katolik">Katolik</option>
                                        <option value="Hindu">Hindu</option>
                                        <option value="Buddha">Buddha</option>
                                        <option value="Konghucu">Konghucu</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4">Pekerjaan</label>
                                <div class="col-md-8">
                                    <input type="text" name="father_job" value="{{old('father_job')}}"
                                           class="form-control form-control-lg @error('father_job') is-invalid @enderror">
                                    @error('father_job')
                                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4">Alamat</label>
                                <div class="col-md-8">
                            <textarea name="father_address" cols="30" rows="3"
                                      class="form-control @error('father_address') is-invalid @enderror">{{old('father_address')}}</textarea>
                                    @error('father_address')
                                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="section-title-underline mb-2">
                                        <span>Identitas Orang Tua (Ibu)</span>
                                    </h4>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4">Nama Ibu</label>
                                <div class="col-md-8">
                                    <input type="text" name="mother_name" value="{{old('mother_name')}}"
                                           class="form-control form-control-lg @error('mother_name') is-invalid @enderror">
                                    @error('mother_name')
                                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4">Tempat Lahir</label>
                                <div class="col-md-8">
                                    <input type="text" name="mother_place_of_birth"
                                           value="{{old('mother_place_of_birth')}}"
                                           class="form-control form-control-lg @error('mother_place_of_birth') is-invalid @enderror">
                                    @error('mother_place_of_birth')
                                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4">Tanggal Lahir</label>
                                <div class="col-md-8">
                                    <input type="text" name="mother_date_of_birth"
                                           value="{{old('mother_date_of_birth')}}"
                                           class="form-control form-control-lg @error('mother_date_of_birth') is-invalid @enderror">
                                    @error('mother_date_of_birth')
                                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4">Agama</label>
                                <div class="col-md-8">
                                    <select name="mother_religion" class="form-control form-control-lg">
                                        <option value="Islam">Islam</option>
                                        <option value="Kristen Protestan">Kristen Protestan</option>
                                        <option value="Katolik">Katolik</option>
                                        <option value="Hindu">Hindu</option>
                                        <option value="Buddha">Buddha</option>
                                        <option value="Konghucu">Konghucu</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4">Pekerjaan</label>
                                <div class="col-md-8">
                                    <input type="text" name="mother_job" value="{{old('mother_job')}}"
                                           class="form-control form-control-lg @error('mother_job') is-invalid @enderror">
                                    @error('mother_job')
                                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4">Alamat</label>
                                <div class="col-md-8">
                            <textarea name="mother_address" cols="30" rows="3"
                                      class="form-control @error('mother_address') is-invalid @enderror">{{old('mother_address')}}</textarea>
                                    @error('mother_address')
                                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <input type="submit" value="Registrasi" class="btn btn-primary btn-lg px-5">
                        </div>
                    </div>
                </form>
            @endif
        @endif
    </div>

@endsection

@section('scripts')
    <script type="application/javascript">
        jQuery(document).ready(function () {
            $('input[name="date_of_birth"]').datepicker({
                uiLibrary: 'bootstrap4', iconsLibrary: 'materialicons'
            });

            $('input[name="father_date_of_birth"]').datepicker({
                uiLibrary: 'bootstrap4', iconsLibrary: 'materialicons'
            });

            $('input[name="mother_date_of_birth"]').datepicker({
                uiLibrary: 'bootstrap4', iconsLibrary: 'materialicons'
            });
        })
    </script>
@endsection