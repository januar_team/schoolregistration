@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> Pendaftar (Tahun Ajaran {{$registration_year->name}})
                    <div class="card-header-actions">
                        <button class="btn btn-primary btn-sm btn-generate">Generate Kelas</button>
                    </div>
                </div>

                <div class="card-body">
                    <table id="datatables" class="table table-responsive-sm table-hover table-outline mb-0"
                           width="100%">
                        <thead class="thead-light">
                        <tr>
                            <th class="center">#</th>
                            <th>Nomor Registrasi</th>
                            <th>Nama</th>
                            <th>NISN</th>
                            <th>Sekolah Asal</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div id="generateModal" class="modal fade" data-backdrop="static" role="dialog" tabindex="-1">
        <div class="modal-dialog" role="document" style="width: 55%;">
            <div class="modal-content">
                <form id="generateModalForm" method="get" action="{{route('admin.registrant.generate', $registration_year->id)}}" novalidate>
                    <div class="modal-header">
                        <h4 class="modal-title">Generate Kelas</h4>
                        <button class="close pull-left" data-dismiss="modal" aria-label="close" type="button"><span
                                aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Apakah anda ingin men-generate kelas?</p>
                    </div>
                    <div class="modal-footer">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-primary">OK</button>
                                </div>
                                <div class="pull-right">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="application/javascript">
        jQuery(document).ready(function () {
            $('#datatables').DataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                bFilter: true,
                lengthChange: true,
                ajax: {
                    url: "/admin/registrant/list/{{$registration_year->id}}",
                    type: 'POST',
                },
                columns: [
                    {
                        data: null,
                        className: 'control',
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        },
                        "width": "20px",
                        "orderable": false,
                        searchable: false
                    },
                    {
                        data: 'registration_number'
                    },
                    {
                        data: 'name'
                    },
                    {
                        data: 'nisn'
                    },
                    {
                        data: 'school_from'
                    },
                    {
                        width: '15%',
                        data: null,
                        orderable: false,
                        searchable: false,
                        className: 'text-right',
                        render: function (data, type, row) {
                            return '<a class="btn btn-primary btn-pill btn-sm" href="/admin/registrant/detail/'+ data.id +'"><i class="fa fa-list"></i> detail</a>';
                        }
                    }
                ]
            });

            $('.btn-generate').click(function (event) {
                event.preventDefault();
                $('#generateModal').modal();
            })
        });
    </script>
@endsection
