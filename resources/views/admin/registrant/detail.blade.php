@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> Data Utama Siswa
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <tr>
                                <td width="30%">Nama Siswa Lengkap (sesuai akte kelahiran)</td>
                                <td width="5%">:</td>
                                <td>{{$registrant->name}}</td>
                            </tr>
                            <tr>
                                <td width="30%">Nama Panggilan</td>
                                <td width="5%">:</td>
                                <td>{{$registrant->nickname}}</td>
                            </tr>
                            <tr>
                                <td width="30%">No. Induk Siswa Nasional (NISN)</td>
                                <td width="5%">:</td>
                                <td>{{$registrant->nisn}}</td>
                            </tr>

                            @if(!is_null($registrant->student))
                                <tr>
                                    <td width="30%">Kelas</td>
                                    <td width="5%">:</td>
                                    <td>{{$registrant->student->class}}</td>
                                </tr>
                            @endif
                        </table>
                    </div>
                </div>
                <div class="card-footer">
                    <a href="{{route('admin.registrant.list', ['registration_year' => $registrant->registration_year_id])}}"
                    class="btn btn-primary btn-sm float-right">
                        <i class="fa fa-arrow-left"></i> Kembali
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 col-lg-6 col-sm-12">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <i class="fa fa-align-justify"></i> Data Umum Siswa
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-borderless">
                                    <tr>
                                        <td>Tempat Lahir</td>
                                        <td>: {{$registrant->place_of_birth}}</td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal Lahir</td>
                                        <td>: {{$registrant->date_of_birth}}</td>
                                    </tr>
                                    <tr>
                                        <td>Jenis Kelamin</td>
                                        <td>: {{$registrant->gender}}</td>
                                    </tr>
                                    <tr>
                                        <td>Agama</td>
                                        <td>: {{$registrant->religion}}</td>
                                    </tr>
                                    <tr>
                                        <td>Kewarganegaraan</td>
                                        <td>: {{$registrant->nationality}}</td>
                                    </tr>
                                    <tr>
                                        <td>Anak Ke-</td>
                                        <td>: {{$registrant->child_num}} dari {{$registrant->child_of}}</td>
                                    </tr>
                                    <tr>
                                        <td>Status Dalam Keluarga</td>
                                        <td>: {{$registrant->family_status}}</td>
                                    </tr>
                                    <tr>
                                        <td>Alamat</td>
                                        <td>: {{$registrant->address}}</td>
                                    </tr>
                                    <tr>
                                        <td>RT/RW</td>
                                        <td>: {{$registrant->rt_rw}}</td>
                                    </tr>
                                    <tr>
                                        <td>Kelurahan</td>
                                        <td>: {{$registrant->village}}</td>
                                    </tr>
                                    <tr>
                                        <td>Kecamatan</td>
                                        <td>: {{$registrant->district}}</td>
                                    </tr>
                                    <tr>
                                        <td>Kota/Kabupatan</td>
                                        <td>: {{$registrant->city}}</td>
                                    </tr>
                                    <tr>
                                        <td>Provinsi</td>
                                        <td>: {{$registrant->province}}</td>
                                    </tr>
                                    <tr>
                                        <td>Kode Pos</td>
                                        <td>: {{$registrant->post_code}}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <i class="fa fa-align-justify"></i> Data Riwayat Pendidikan
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-borderless">
                                    <tr>
                                        <td>Sekolah Asal</td>
                                        <td>: {{$registrant->school_from}}</td>
                                    </tr>
                                    <tr>
                                        <td>Alamat Sekolah Asal</td>
                                        <td>: {{$registrant->school_from_address}}</td>
                                    </tr>
                                    <tr>
                                        <td>No. STTB</td>
                                        <td>: {{$registrant->sttb}}</td>
                                    </tr>
                                    <tr>
                                        <td>Tahun Lulus</td>
                                        <td>: {{$registrant->graduation_year}}</td>
                                    </tr>
                                    <tr>
                                        <td>Nomor SKHUN</td>
                                        <td>: {{$registrant->skhun}}</td>
                                    </tr>
                                    <tr>
                                        <td>Jumlah Nilai STTB/ Rata-Rata</td>
                                        <td>: {{$registrant->sttb_mark}}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-6 col-sm-12">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <i class="fa fa-align-justify"></i> Identitas Orang Tua (Ayah)
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-borderless">
                                    <tr>
                                        <td>Nama Ayah</td>
                                        <td>: {{$registrant->father_name}}</td>
                                    </tr>
                                    <tr>
                                        <td>Tempat Lahir</td>
                                        <td>: {{$registrant->father_place_of_birth}}</td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal Lahir</td>
                                        <td>: {{$registrant->father_date_of_birth}}</td>
                                    </tr>
                                    <tr>
                                        <td>Agama</td>
                                        <td>: {{$registrant->father_religion}}</td>
                                    </tr>
                                    <tr>
                                        <td>Pekerjaan</td>
                                        <td>: {{$registrant->father_job}}</td>
                                    </tr>
                                    <tr>
                                        <td>Alamar</td>
                                        <td>: {{$registrant->father_address}}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <i class="fa fa-align-justify"></i> Identitas Orang Tua (Ibu)
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-borderless">
                                    <tr>
                                        <td>Nama Ibu</td>
                                        <td>: {{$registrant->mother_name}}</td>
                                    </tr>
                                    <tr>
                                        <td>Tempat Lahir</td>
                                        <td>: {{$registrant->mother_place_of_birth}}</td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal Lahir</td>
                                        <td>: {{$registrant->mother_date_of_birth}}</td>
                                    </tr>
                                    <tr>
                                        <td>Agama</td>
                                        <td>: {{$registrant->mother_religion}}</td>
                                    </tr>
                                    <tr>
                                        <td>Pekerjaan</td>
                                        <td>: {{$registrant->mother_job}}</td>
                                    </tr>
                                    <tr>
                                        <td>Alamar</td>
                                        <td>: {{$registrant->mother_address}}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="application/javascript">
        jQuery(document).ready(function () {
        });
    </script>
@endsection
