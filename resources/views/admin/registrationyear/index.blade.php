@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> Tahun Ajaran
                    <div class="card-header-actions">
                        <button class="btn btn-primary btn-sm btn-add">
                            <i class="icon-plus"></i> Tambah
                        </button>
                    </div>
                </div>

                <div class="card-body">
                    <table id="datatables" class="table table-responsive-sm table-hover table-outline mb-0"
                           width="100%">
                        <thead class="thead-light">
                        <tr>
                            <th class="center">#</th>
                            <th>Tahun Ajaran</th>
                            <th>Tanggal Mulai</th>
                            <th>Tanggal Akhir</th>
                            <th>Maksimal Pendaftar</th>
                            <th>Jumlah Kelas</th>
                            <th>Minimal Siswa/Kelas</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div id="addModal" class="modal fade" data-backdrop="static" role="dialog" tabindex="-1">
            <div class="modal-dialog" role="document" style="width: 55%;">
                <div class="modal-content">
                    <form id="addModalForm" method="post" action="" novalidate>
                        <div class="modal-header">
                            <h4 class="modal-title">Tahun Ajaran</h4>
                            <button class="close pull-left" data-dismiss="modal" aria-label="close" type="button"><span
                                        aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="name">Tahun Ajaran</label>
                                <input type="text" class="form-control name-add-input" name="name" value=""
                                       autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"/>
                            </div>

                            <div class="form-group">
                                <label for="start_date">Durasi</label>
                                <input type="text" class="form-control" name="duration"
                                       data-start-date="{{\Carbon\Carbon::now()->format('d/m/Y')}}"
                                       autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"/>
                            </div>

                            <div class="form-group">
                                <label for="max_registrant">Maksimal Pendaftar</label>
                                <input type="number" class="form-control" name="max_registrant"
                                       autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"/>
                            </div>

                            <div class="form-group">
                                <label for="class_count">Jumlah Kelas</label>
                                <input type="number" class="form-control" name="class_count"
                                       autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"/>
                            </div>

                            <div class="form-group">
                                <label for="min_student_count">Minimal Siswa/Kelas</label>
                                <input type="number" class="form-control" name="min_student_count"
                                       autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"/>
                            </div>

                            <div id="error_message" class="form-group"></div>
                        </div>
                        <div class="modal-footer">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="pull-right">
                                        <button type="submit" class="btn btn-primary">Simpan</button>
                                    </div>
                                    <div class="pull-right">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div id="deleteModal" class="modal fade" data-backdrop="static" role="dialog" tabindex="-1">
            <div class="modal-dialog" role="document" style="width: 55%;">
                <div class="modal-content">
                    <form id="deleteModalForm" method="post" action="{{route('admin.registration.year.delete')}}" novalidate>
                        <div class="modal-header">
                            <h4 class="modal-title">Hapus Tahun Ajaran</h4>
                            <button class="close pull-left" data-dismiss="modal" aria-label="close" type="button"><span
                                        aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <input name="id" type="hidden">
                            <p>Apakah anda ingin menhapus <b id="item-name"></b></p>
                            <div id="error_message" class="form-group"></div>
                        </div>
                        <div class="modal-footer">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="pull-right">
                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                    </div>
                                    <div class="pull-right">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="application/javascript">
        jQuery(document).ready(function () {
            $('input[name="duration"]').daterangepicker({
                minDate : moment(),
                locale : {
                    format : 'DD/MM/YYYY'
                }
            });
            let table = $('#datatables');
            let datatable = table.DataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                bFilter: true,
                lengthChange: true,
                ajax: {
                    url: "/admin/registration_year",
                    type: 'POST',
                },
                columns: [
                    {
                        data: null,
                        className: 'control',
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        },
                        "width": "20px",
                        "orderable": false,
                        searchable: false
                    },
                    {
                        data: 'name'
                    },
                    {
                        data: 'start_date'
                    },
                    {
                        data: 'end_date'
                    },
                    {
                        data: 'max_registrant',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'class_count',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'min_student_count',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: null,
                        orderable: false,
                        searchable: false,
                        render : function (data, type , row) {
                            return '<label class="switch switch-label switch-pill switch-outline-primary-alt">' +
                                '<input class="switch-input item-status" type="checkbox" '+ ((data.status) ? "checked" : "") +' value="'+ data.id +'">' +
                                '<span class="switch-slider" data-checked="On" data-unchecked="Off"></span>' +
                                '</label>';
                        }
                    },
                    {
                        width: '15%',
                        data: null,
                        orderable: false,
                        searchable: false,
                        className: 'text-right',
                        render: function (data, type, row) {
                            return '<div role="group" class="btn-group-sm btn-group" data-item=\''+ JSON.stringify(data) +'\'>' +
                                '<button type="button" data-toggle="tooltip" data-placement="top" title="Edit" class="btn btn-warning btn-edit">' +
                                '<i class="fa fa-edit"></i></button>' +
                                '<button type="button" data-toggle="tooltip" data-placement="top" title="Delete" class="btn btn-danger btn-delete">' +
                                '<i class="fa fa-trash"></i></button>' +
                                '</div>';
                        }
                    }
                ]
            });

            table.on('draw.dt', () => {
                table.find('.btn').tooltip();
            });

            $('.btn-add').click(function (event) {
                event.preventDefault();
                let form = $('#addModalForm');
                form.attr('action', "/admin/registration_year/add");
                window.resetForm(form);
                form[0].reset();
                $('#addModal').modal();
            });

            table.on('click','.btn-edit', function (event) {
                event.preventDefault();
                let form = $('#addModalForm');
                let data = $(this).parent().data('item');
                window.setValue(form, data);
                $('input[name="duration"]').data('daterangepicker').setStartDate(data.start_date);
                $('input[name="duration"]').data('daterangepicker').setEndDate(data.end_date);
                form.attr('action', '/admin/registration_year/edit/' + data.id);
                window.resetForm(form);
                $('#addModal').modal();
            });

            table.on('click','.btn-delete', function (event) {
                event.preventDefault();
                let form = $('#deleteModalForm');
                let data = $(this).parent().data('item');
                form.find('input[name="id"]').val(data.id);
                form.find('#item-name').html(data.name);
                window.resetForm(form);
                $('#deleteModal').modal();
            });

            table.on('change','.item-status', function (event) {
                let status = $(this).is(':checked');
                let item = $(this);
                $.ajax({
                    url: '/admin/registration_year/status/' + $(this).val(),
                    type: 'POST',
                    data: {
                        status : status
                    },
                    cache: false,
                    success: function (data) {
                        if (data.status) {
                            datatable.draw();
                            toastr.success(data.message, 'Success');
                        } else {
                            toastr.error(data.message, 'Error');
                        }
                    },
                    error: function (xhr) {
                        window.handleError(xhr, item);
                        item.prop('checked', !status);
                    },
                    complete: function () {
                    }
                });
            });

            $('#addModalForm').submit(function (event) {
                event.preventDefault();
                $('#addModal button[type=submit]').button_bs4('loading');
                let form = $(this);
                let _data = form.serialize();
                window.resetForm(form);
                $.ajax({
                    url: form.attr('action'),
                    type: 'POST',
                    data: _data,
                    cache: false,
                    success: function (data) {
                        if (data.status) {
                            $('#addModal').modal('hide');
                            form[0].reset();
                            datatable.draw();
                            toastr.success(data.message, 'Success');
                        } else {
                            toastr.error(data.message, 'Error');
                        }
                    },
                    error: function (xhr) {
                        window.handleError(xhr, form);
                    },
                    complete: function () {
                        $('#addModal button[type=submit]').button_bs4('reset');
                    }
                });
            });

            $('#deleteModalForm').submit(function (event) {
                event.preventDefault();
                $('#deleteModal button[type=submit]').button_bs4('loading');
                let form = $(this);
                let _data = form.serialize();
                window.resetForm(form);
                $.ajax({
                    url: form.attr('action'),
                    type: 'POST',
                    data: _data,
                    cache: false,
                    success: function (data) {
                        if (data.status) {
                            $('#deleteModal').modal('hide');
                            form[0].reset();
                            datatable.draw();
                            toastr.success(data.message, 'Success');
                        } else {
                            toastr.error(data.message, 'Error');
                        }
                    },
                    error: function (xhr) {
                        window.handleError(xhr, form);
                    },
                    complete: function () {
                        $('#deleteModal button[type=submit]').button_bs4('reset');
                    }
                });
            });
        });
    </script>
@endsection
