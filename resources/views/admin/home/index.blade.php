@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-4">
            <div class="card text-white bg-success">
                <div class="card-body">
                    <div class="h1 text-muted text-right mb-4">
                        <i class="icon-user-follow"></i>
                    </div>
                    <div class="text-value">{{number_format($current_registrant)}}</div>
                    <small class="text-muted text-uppercase font-weight-bold">Jumlah Pendaftar Saat Ini</small>
                    <div class="progress progress-white progress-xs mt-3">
                        <div class="progress-bar" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-4">
            <div class="card text-white bg-primary">
                <div class="card-body">
                    <div class="h1 text-muted text-right mb-4">
                        <i class="icon-user-follow"></i>
                    </div>
                    <div class="text-value">{{number_format($students)}}</div>
                    <small class="text-muted text-uppercase font-weight-bold">Jumlah Siswa</small>
                    <div class="progress progress-white progress-xs mt-3">
                        <div class="progress-bar" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-4">
            <div class="card text-white bg-danger">
                <div class="card-body">
                    <div class="h1 text-muted text-right mb-4">
                        <i class="icon-user-follow"></i>
                    </div>
                    <div class="text-value">{{number_format($students_current)}}</div>
                    <small class="text-muted text-uppercase font-weight-bold">Jumlah Siswa Periode Terakhir</small>
                    <div class="progress progress-white progress-xs mt-3">
                        <div class="progress-bar" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
