@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> Pendaftar
                </div>

                <div class="card-body">
                    <table id="datatables" class="table table-responsive-sm table-hover table-outline mb-0"
                           width="100%">
                        <thead class="thead-light">
                        <tr>
                            <th class="center">#</th>
                            <th>Tahun Ajaran</th>
                            <th>Jumlah Kelas</th>
                            <th>Jumlah Siswa</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="application/javascript">
        jQuery(document).ready(function () {
            $('#datatables').DataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                bFilter: true,
                lengthChange: true,
                ajax: {
                    url: "/admin/students",
                    type: 'POST',
                },
                columns: [
                    {
                        data: null,
                        className: 'control',
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        },
                        "width": "20px",
                        "orderable": false,
                        searchable: false
                    },
                    {
                        data: 'name'
                    },
                    {
                        data: 'class_count'
                    },
                    {
                        data: 'students_count',
                        orderable: false,
                        searchable: false
                    },
                    {
                        width: '15%',
                        data: null,
                        orderable: false,
                        searchable: false,
                        className: 'text-right',
                        render: function (data, type, row) {
                            return '<a class="btn btn-primary btn-pill btn-sm" href="/admin/students/list/'+ data.id +'"><i class="fa fa-list"></i> detail</a>';
                        }
                    }
                ]
            });

        });
    </script>
@endsection
