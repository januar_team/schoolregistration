@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <form method="post">
                    @csrf
                    <div class="card">
                        <div class="card-header">
                            <i class="fa fa-align-justify"></i> About Us
                        </div>

                        <div class="card-body">
                            <textarea name="page" id="editor">{{$page->value}}</textarea>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary float-right">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="application/javascript">
        ClassicEditor
            .create( document.querySelector( '#editor' ),{
                toolbar : ["undo", "redo", "bold", "italic", "blockQuote", "ckfinder", "imageTextAlternative", "imageUpload", "heading", "imageStyle:full", "imageStyle:side", "link", "numberedList", "bulletedList", "mediaEmbed", "insertTable", "tableColumn", "tableRow", "mergeTableCells"]
            } )
            .then( editor => {
                console.log( Array.from( editor.ui.componentFactory.names() ) );
            } )
            .catch( error => {
            } );
    </script>
@endsection
