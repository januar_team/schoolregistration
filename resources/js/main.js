window._ = require('lodash');

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
    window.Popper = require('popper.js').default;
    window.$ = window.jQuery = require('jquery');

    require('bootstrap');
} catch (e) {}

require('jquery-migrate');
require('owl.carousel');
require('./plugin/jquery.stellar.min');
require('jquery-countdown');
require('jquery.easing');
window.AOS = require('aos');
require('@fancyapps/fancybox');
require('jquery-sticky');
require('./web');
require('gijgo');