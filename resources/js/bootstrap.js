window._ = require('lodash');

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
    window.Popper = require('popper.js').default;
    window.$ = window.jQuery = require('jquery');

    require('bootstrap');
} catch (e) {
}

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

/*window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';*/

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    // window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
    $(document).ready(function ($) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(document).ajaxError(function (event, jqxhr, settings, thrownError) {
            if (jqxhr.status === 401 || jqxhr.status === 419) {
                document.location.reload();
            } else if (jqxhr.status === 403) {
                $.toast({
                    heading: 'Error',
                    text: (jqxhr.responseJSON !== undefined) ? jqxhr.responseJSON.message : jqxhr.statusText,
                    position: 'top-right',
                    icon: 'error'
                })
            }
        });
    });
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

window.moment = require('moment');

require('@coreui/coreui');
require('@coreui/coreui/js/dist/ajax-load');
require('@coreui/coreui/js/dist/sidebar');
require('chart.js');
require('@coreui/coreui-plugin-chartjs-custom-tooltips/dist/js/custom-tooltips');

require('select2');
require('icheck');
require('datatables.net-bs4');
require('datatables.net-responsive-bs4');
require('bootstrap-datepicker');
require('daterangepicker');
window.ClassicEditor = require('@ckeditor/ckeditor5-build-classic');
require('./button_bs4');

window.toastr = require('toastr');

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from 'laravel-echo'

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: process.env.MIX_PUSHER_APP_KEY,
//     cluster: process.env.MIX_PUSHER_APP_CLUSTER,
//     encrypted: true
// });

window.resetForm = function (form) {
    form.find('.form-control').removeClass('is-invalid');
    form.find('.invalid-feedback').remove();
};

window.handleError = function (response, form) {
    if (response.readyState === 4) {
        if (response.status === 422 || response.status === 423) {
            var errors = response.responseJSON.errors;
            $.each(errors, function (key, error) {
                var item = form.find('input[name=' + key + ']');
                item = (item.length > 0) ? item : form.find('select[name=' + key + ']');
                item = (item.length > 0) ? item : form.find('textarea[name=' + key + ']');
                item = (item.length > 0) ? item : form.find("input[name='" + key + "[]']");
                item.addClass('is-invalid');
                item.parent().append('<span class="invalid-feedback">' + error + '</span>');
            })
        } else if (response.responseJSON.status === false) {
            if (form.find('#error_message').length > 0) {
                form.find('#error_message').addClass('has-error');
                form.find('#error_message').append('<span class="invalid-feedback">' + response.responseJSON.message + '</span>');
            } else {
                toastr.error(response.responseJSON.message, 'Error');
            }
        } else {
            if (form.find('#error_message').length > 0) {
                form.find('#error_message').addClass('has-error');
                form.find('#error_message').append('<span class="invalid-feedback">Whoops, looks like something went wrong.</span>');
            } else {
                toastr.error('Whoops, looks like something went wrong.', 'Error');
            }
        }
    } else if (response.readyState === 0) {
        if (form.find('#error_message').length > 0) {
            form.find('#error_message').addClass('has-error');
            form.find('#error_message').append('<span class="invalid-feedback">Request failed, please check your connection</span>');
        } else {
            toastr.error('Request failed, please check your connection', 'Error');
        }
    }
};

window.setValue = (form, values) => {
    $.each(values, (index, value) => {
        var item = form.find('input[name=' + index + ']');
        item = (item.length > 0) ? item : form.find('select[name=' + index + ']');
        item = (item.length > 0) ? item : form.find('textarea[name=' + index + ']');
        item = (item.length > 0) ? item : form.find("input[name='" + index + "[]']");
        item.val(value)
    })
};
