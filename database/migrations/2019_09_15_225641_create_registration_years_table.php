<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistrationYearsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registration_years', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->date('start_date');
            $table->date('end_date');
            $table->boolean('status')->default(false);
            $table->integer('max_registrant');
            $table->integer('class_count');
            $table->integer('min_student_count');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registration_years');
    }
}
