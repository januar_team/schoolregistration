<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistrantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registrants', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('registration_year_id');
            $table->string('registration_number')->unique();
            $table->string('name');
            $table->string('nickname');
            $table->string('nisn');
            $table->string('place_of_birth');
            $table->date('date_of_birth');
            $table->enum('gender', ['laki-laki', 'perempuan']);
            $table->string('religion');
            $table->string('nationality');
            $table->integer('child_num');
            $table->integer('child_of');
            $table->string('family_status');
            $table->text('address');
            $table->string('rt_rw');
            $table->string('village');
            $table->string('district');
            $table->string('city');
            $table->string('province');
            $table->string('post_code');
            $table->string('school_from');
            $table->text('school_from_address');
            $table->string('sttb');
            $table->string('graduation_year');
            $table->string('skhun');
            $table->decimal('sttb_mark');
            $table->string('father_name');
            $table->string('father_place_of_birth');
            $table->date('father_date_of_birth');
            $table->string('father_religion');
            $table->string('father_job');
            $table->string('father_address');
            $table->string('mother_name');
            $table->string('mother_place_of_birth');
            $table->date('mother_date_of_birth');
            $table->string('mother_religion');
            $table->string('mother_job');
            $table->string('mother_address');
            $table->timestamps();

            $table->foreign('registration_year_id')->references('id')->on('registration_years');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registrants');
    }
}
