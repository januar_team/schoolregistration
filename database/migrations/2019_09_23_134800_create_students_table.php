<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('registration_year_id');
            $table->unsignedBigInteger('registrant_id');
            $table->string('class');
            $table->timestamps();

            $table->foreign('registration_year_id')->on('registration_years')->references('id');
            $table->foreign('registrant_id')->on('registrants')->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
