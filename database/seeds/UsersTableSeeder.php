<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::firstOrCreate(
            [
                'username'  => 'superadmin'
            ],
            [
                'name'      => 'Superadmin',
                'password'  => bcrypt('admin2121')
            ]
        );
    }
}
